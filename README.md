# webapp-playbook to deploy python webapp 

Name of webapp- https://bitbucket.org/azneita/devops-challenge.git

* How to run ansible playbook manually on local vm

     # git clone https://arunbagul@bitbucket.org/arunbagul/webapp-playbook.git

     # cd webapp-playbook

     # ansible-playbook --connection=local  playbook.yml

* How to run playbook using Vagrant.

  `# vagrant provision`
   
  Please make sure to use ansible provision in vagrant config file

